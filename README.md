# Welcome to Hello C# - CI

This is simple demo of Windows [GitLab CI](https://docs.gitlab.com/ee/ci/)
for simple .NET 4 Console application. It supports following
build matrix:

- Platform: Any CPU, x86, x64
- Configuration: Debug, Release

This project was created
in [Visual Studio 2019 Community](https://visualstudio.microsoft.com/vs/community/)
with `.NET Desktop Development` workload installed.
Additionally you should also install [PowerShell Core](https://github.com/powershell/powershell)

To run this project with [GitLab CI](https://docs.gitlab.com/ee/ci/) you need to have:
- [Windows with gitlab-runner](https://docs.gitlab.com/runner/install/windows.html)
- Runner must have registered `shell` of type `pwsh`
  known as [PowerShell Core](https://github.com/powershell/powershell)
- runner has to offer `powershell` tag.

Please see `.gitlab-ci.yml` file for GitLab CI definition for this project.