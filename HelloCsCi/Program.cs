﻿using System;

namespace HelloCsCi
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine($"Hello, C# Console! Today is {DateTime.UtcNow} ");
        }
    }
}
